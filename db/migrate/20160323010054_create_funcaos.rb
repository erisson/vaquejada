class CreateFuncaos < ActiveRecord::Migration
  def change
    create_table :funcaos do |t|
      t.text :descricao
      t.string :tipo

      t.timestamps null: false
    end
  end
end
