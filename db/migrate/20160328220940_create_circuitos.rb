class CreateCircuitos < ActiveRecord::Migration
  def change
    create_table :circuitos do |t|
      t.string :cidade
      t.string :estado
      t.integer :ano
      t.string :contato
      t.string :telefone_contato
      t.string :telefone_contato_2
      t.text :descricao

      t.timestamps null: false
    end
  end
end
