class CreateEventoVaquejadas < ActiveRecord::Migration
  def change
    create_table :evento_vaquejadas do |t|
      t.text :descricao
      t.datetime :data_inicio
      t.datetime :data_fim
      t.string :cidade
      t.string :estado
      t.references :circuito, index: true, foreign_key: true
      t.integer :qtd_escricoes
      t.references :operador, index: true, foreign_key: true
      t.integer :qtd_bois_profissional
      t.integer :qtd_bois_amador
      t.string :tipo_senha
      t.integer :qtd_escricoes_vaqueiro

      t.timestamps null: false
    end
  end
end
