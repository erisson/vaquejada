class CreateVaqueiros < ActiveRecord::Migration
  def change
    create_table :vaqueiros do |t|
      t.string :nome
      t.string :cpf
      t.string :tipo
      t.datetime :data_nascimento
      t.string :cidade
      t.string :estado

      t.timestamps null: false
    end
  end
end
