class CreateOperadors < ActiveRecord::Migration
  def change
    create_table :operadors do |t|
      t.string :nome
      t.string :nome_reduzido
      t.references :funcao, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
