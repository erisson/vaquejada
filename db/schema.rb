# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160506162816) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "circuitos", force: :cascade do |t|
    t.string   "cidade"
    t.string   "estado"
    t.integer  "ano"
    t.string   "contato"
    t.string   "telefone_contato"
    t.string   "telefone_contato_2"
    t.text     "descricao"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "nome"
  end

  create_table "evento_vaquejadas", force: :cascade do |t|
    t.text     "descricao"
    t.datetime "data_inicio"
    t.datetime "data_fim"
    t.string   "cidade"
    t.string   "estado"
    t.integer  "circuito_id"
    t.integer  "qtd_escricoes"
    t.integer  "operador_id"
    t.integer  "qtd_bois_profissional"
    t.integer  "qtd_bois_amador"
    t.string   "tipo_senha"
    t.integer  "qtd_escricoes_vaqueiro"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "operador2_id"
  end

  add_index "evento_vaquejadas", ["circuito_id"], name: "index_evento_vaquejadas_on_circuito_id", using: :btree
  add_index "evento_vaquejadas", ["operador_id"], name: "index_evento_vaquejadas_on_operador_id", using: :btree

  create_table "funcaos", force: :cascade do |t|
    t.text     "descricao"
    t.string   "tipo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "operadors", force: :cascade do |t|
    t.string   "nome"
    t.string   "nome_reduzido"
    t.integer  "funcao_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "operadors", ["funcao_id"], name: "index_operadors_on_funcao_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vaqueiros", force: :cascade do |t|
    t.string   "nome"
    t.string   "cpf"
    t.string   "tipo"
    t.datetime "data_nascimento"
    t.string   "cidade"
    t.string   "estado"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_foreign_key "evento_vaquejadas", "circuitos"
  add_foreign_key "evento_vaquejadas", "operadors"
  add_foreign_key "operadors", "funcaos"
end
