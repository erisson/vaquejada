json.array!(@evento_vaquejadas) do |evento_vaquejada|
  json.extract! evento_vaquejada, :id, :descricao, :data_inicio, :data_fim, :cidade, :estado, :circuito_id, :qtd_escricoes, :operador_id, :qtd_bois_profissional, :qtd_bois_amador, :tipo_senha, :qtd_escricoes_vaqueiro
  json.url evento_vaquejada_url(evento_vaquejada, format: :json)
end
