json.array!(@circuitos) do |circuito|
  json.extract! circuito, :id, :nome, :cidade, :estado, :ano, :contato, :telefone_contato, :telefone_contato_2, :descricao
  json.url circuito_url(circuito, format: :json)
end
