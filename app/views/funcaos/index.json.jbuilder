json.array!(@funcaos) do |funcao|
  json.extract! funcao, :id, :descricao, :tipo
  json.url funcao_url(funcao, format: :json)
end
