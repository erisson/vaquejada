json.array!(@vaqueiros) do |vaqueiro|
  json.extract! vaqueiro, :id, :nome, :cpf, :tipo, :data_nascimento, :cidade, :estado
  json.url vaqueiro_url(vaqueiro, format: :json)
end
