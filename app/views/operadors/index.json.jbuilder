json.array!(@operadors) do |operador|
  json.extract! operador, :id, :nome, :nome_reduzido, :funcao_id
  json.url operador_url(operador, format: :json)
end
