class Circuito < ActiveRecord::Base
	validates_presence_of :nome, :cidade, :estado, :ano

	has_many :evento_vaquejada, :dependent => :restrict_with_error 
end
