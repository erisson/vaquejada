class EventoVaquejada < ActiveRecord::Base
  belongs_to :circuito
  belongs_to :operador

  belongs_to :operador2,
  :class_name => "Operador"

  validates_presence_of :data_inicio, :data_fim, :cidade, 
  :estado, :circuito_id, :qtd_escricoes, :qtd_bois_profissional, :qtd_bois_amador, :operador_id

  
end
