class Operador < ActiveRecord::Base
  belongs_to :funcao
  validates_presence_of :nome

  has_many :evento_vaquejada, :dependent => :restrict_with_error 

end
