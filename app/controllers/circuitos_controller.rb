class CircuitosController < ApplicationController
  before_action :set_circuito, only: [:show, :edit, :update, :destroy]
  
  # GET /circuitos
  # GET /circuitos.json
  def index
    @circuitos = Circuito.all
  end

  # GET /circuitos/1
  # GET /circuitos/1.json
  def show
  end

  # GET /circuitos/new
  def new
    @circuito = Circuito.new
  end

  # GET /circuitos/1/edit
  def edit
  end

  # POST /circuitos
  # POST /circuitos.json
  def create
    @circuito = Circuito.new(circuito_params)

    respond_to do |format|
      if @circuito.save
        format.html { redirect_to @circuito, notice: 'Circuito foi criado com sucesso.' }
        format.json { render :show, status: :created, location: @circuito }
      else
        format.html { render :new }
        format.json { render json: @circuito.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /circuitos/1
  # PATCH/PUT /circuitos/1.json
  def update
    respond_to do |format|
      if @circuito.update(circuito_params)
        format.html { redirect_to @circuito, notice: 'Circuito foi atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @circuito }
      else
        format.html { render :edit }
        format.json { render json: @circuito.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /circuitos/1
  # DELETE /circuitos/1.json
  def destroy
    respond_to do |format|
      if @circuito.destroy
        format.html { redirect_to circuitos_url, notice: 'Circuito foi excluído com sucesso.' }
      else    
        format.html { redirect_to circuitos_url, alert: @circuito.errors.messages[:base]}
      end  
        format.json { head :no_content }  
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_circuito
      @circuito = Circuito.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def circuito_params
      params.require(:circuito).permit(:nome, :cidade, :estado, :ano, :contato, :telefone_contato, :telefone_contato_2, :descricao)
    end
end
