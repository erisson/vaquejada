class VaqueirosController < ApplicationController
  before_action :set_vaqueiro, only: [:show, :edit, :update, :destroy]
  
  # GET /vaqueiros
  # GET /vaqueiros.json
  def index
    @vaqueiros = Vaqueiro.all
  end

  # GET /vaqueiros/1
  # GET /vaqueiros/1.json
  def show
  end

  # GET /vaqueiros/new
  def new
    @vaqueiro = Vaqueiro.new
  end

  # GET /vaqueiros/1/edit
  def edit
  end

  # POST /vaqueiros
  # POST /vaqueiros.json
  def create
    @vaqueiro = Vaqueiro.new(vaqueiro_params)

    respond_to do |format|
      if @vaqueiro.save
        format.html { redirect_to @vaqueiro, notice: 'Vaqueiro foi criado com sucesso.' }
        format.json { render :show, status: :created, location: @vaqueiro }
      else
        format.html { render :new }
        format.json { render json: @vaqueiro.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vaqueiros/1
  # PATCH/PUT /vaqueiros/1.json
  def update
    respond_to do |format|
      if @vaqueiro.update(vaqueiro_params)
        format.html { redirect_to @vaqueiro, notice: 'Vaqueiro foi atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @vaqueiro }
      else
        format.html { render :edit }
        format.json { render json: @vaqueiro.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vaqueiros/1
  # DELETE /vaqueiros/1.json
  def destroy
    @vaqueiro.destroy
    respond_to do |format|
      format.html { redirect_to vaqueiros_url, notice: 'Vaqueiro foi excluído com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vaqueiro
      @vaqueiro = Vaqueiro.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vaqueiro_params
      params.require(:vaqueiro).permit(:nome, :cpf, :tipo, :data_nascimento, :cidade, :estado)
    end
end
