class EventoVaquejadasController < ApplicationController
  before_action :set_evento_vaquejada, only: [:show, :edit, :update, :destroy]
  
  # GET /evento_vaquejadas
  # GET /evento_vaquejadas.json
  def index
    @evento_vaquejadas = EventoVaquejada.all
  end

  # GET /evento_vaquejadas/1
  # GET /evento_vaquejadas/1.json
  def show
  end

  # GET /evento_vaquejadas/new
  def new
    @evento_vaquejada = EventoVaquejada.new
  end

  # GET /evento_vaquejadas/1/edit
  def edit
  end

  # POST /evento_vaquejadas
  # POST /evento_vaquejadas.json
  def create
    @evento_vaquejada = EventoVaquejada.new(evento_vaquejada_params)

    respond_to do |format|
      if @evento_vaquejada.save
        format.html { redirect_to @evento_vaquejada, notice: 'Evento vaquejada foi criado com sucesso.' }
        format.json { render :show, status: :created, location: @evento_vaquejada }
      else
        format.html { render :new }
        format.json { render json: @evento_vaquejada.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /evento_vaquejadas/1
  # PATCH/PUT /evento_vaquejadas/1.json
  def update
    respond_to do |format|
     
      if @evento_vaquejada.update(evento_vaquejada_params)
        format.html { redirect_to @evento_vaquejada, notice: 'Evento vaquejada foi atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @evento_vaquejada }
      else
        format.html { render :edit }
        format.json { render json: @evento_vaquejada.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /evento_vaquejadas/1
  # DELETE /evento_vaquejadas/1.json
  def destroy
    @evento_vaquejada.destroy
    respond_to do |format|
      format.html { redirect_to evento_vaquejadas_url, notice: 'Evento vaquejada foi excluído com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_evento_vaquejada
      @evento_vaquejada = EventoVaquejada.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def evento_vaquejada_params
      params.require(:evento_vaquejada).permit(:operador_id, :operador2_id, :descricao, :data_inicio, :data_fim, :cidade, :estado, :circuito_id, :qtd_escricoes, :operadors, :qtd_bois_profissional, :qtd_bois_amador, :tipo_senha, :qtd_escricoes_vaqueiro)
    end
end
