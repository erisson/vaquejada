# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!

#Configuracao para mostrar data no show
Time::DATE_FORMATS[:custom_datetime] = "%d/%m/%Y"
