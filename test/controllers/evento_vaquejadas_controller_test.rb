require 'test_helper'

class EventoVaquejadasControllerTest < ActionController::TestCase
  setup do
    @evento_vaquejada = evento_vaquejadas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:evento_vaquejadas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create evento_vaquejada" do
    assert_difference('EventoVaquejada.count') do
      post :create, evento_vaquejada: { cidade: @evento_vaquejada.cidade, circuito_id: @evento_vaquejada.circuito_id, data_fim: @evento_vaquejada.data_fim, data_inicio: @evento_vaquejada.data_inicio, descricao: @evento_vaquejada.descricao, estado: @evento_vaquejada.estado, operador_id: @evento_vaquejada.operador_id, qtd_bois_amador: @evento_vaquejada.qtd_bois_amador, qtd_bois_profissional: @evento_vaquejada.qtd_bois_profissional, qtd_escricoes: @evento_vaquejada.qtd_escricoes, qtd_escricoes_vaqueiro: @evento_vaquejada.qtd_escricoes_vaqueiro, tipo_senha: @evento_vaquejada.tipo_senha }
    end

    assert_redirected_to evento_vaquejada_path(assigns(:evento_vaquejada))
  end

  test "should show evento_vaquejada" do
    get :show, id: @evento_vaquejada
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @evento_vaquejada
    assert_response :success
  end

  test "should update evento_vaquejada" do
    patch :update, id: @evento_vaquejada, evento_vaquejada: { cidade: @evento_vaquejada.cidade, circuito_id: @evento_vaquejada.circuito_id, data_fim: @evento_vaquejada.data_fim, data_inicio: @evento_vaquejada.data_inicio, descricao: @evento_vaquejada.descricao, estado: @evento_vaquejada.estado, operador_id: @evento_vaquejada.operador_id, qtd_bois_amador: @evento_vaquejada.qtd_bois_amador, qtd_bois_profissional: @evento_vaquejada.qtd_bois_profissional, qtd_escricoes: @evento_vaquejada.qtd_escricoes, qtd_escricoes_vaqueiro: @evento_vaquejada.qtd_escricoes_vaqueiro, tipo_senha: @evento_vaquejada.tipo_senha }
    assert_redirected_to evento_vaquejada_path(assigns(:evento_vaquejada))
  end

  test "should destroy evento_vaquejada" do
    assert_difference('EventoVaquejada.count', -1) do
      delete :destroy, id: @evento_vaquejada
    end

    assert_redirected_to evento_vaquejadas_path
  end
end
