require 'test_helper'

class CircuitosControllerTest < ActionController::TestCase
  setup do
    @circuito = circuitos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:circuitos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create circuito" do
    assert_difference('Circuito.count') do
      post :create, circuito: { ano: @circuito.ano, cidade: @circuito.cidade, contato: @circuito.contato, descricao: @circuito.descricao, estado: @circuito.estado, telefone_contato: @circuito.telefone_contato, telefone_contato_2: @circuito.telefone_contato_2 }
    end

    assert_redirected_to circuito_path(assigns(:circuito))
  end

  test "should show circuito" do
    get :show, id: @circuito
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @circuito
    assert_response :success
  end

  test "should update circuito" do
    patch :update, id: @circuito, circuito: { ano: @circuito.ano, cidade: @circuito.cidade, contato: @circuito.contato, descricao: @circuito.descricao, estado: @circuito.estado, telefone_contato: @circuito.telefone_contato, telefone_contato_2: @circuito.telefone_contato_2 }
    assert_redirected_to circuito_path(assigns(:circuito))
  end

  test "should destroy circuito" do
    assert_difference('Circuito.count', -1) do
      delete :destroy, id: @circuito
    end

    assert_redirected_to circuitos_path
  end
end
