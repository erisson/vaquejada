require 'test_helper'

class VaqueirosControllerTest < ActionController::TestCase
  setup do
    @vaqueiro = vaqueiros(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:vaqueiros)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create vaqueiro" do
    assert_difference('Vaqueiro.count') do
      post :create, vaqueiro: { cidade: @vaqueiro.cidade, cpf: @vaqueiro.cpf, data_nascimento: @vaqueiro.data_nascimento, estado: @vaqueiro.estado, nome: @vaqueiro.nome, tipo: @vaqueiro.tipo }
    end

    assert_redirected_to vaqueiro_path(assigns(:vaqueiro))
  end

  test "should show vaqueiro" do
    get :show, id: @vaqueiro
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @vaqueiro
    assert_response :success
  end

  test "should update vaqueiro" do
    patch :update, id: @vaqueiro, vaqueiro: { cidade: @vaqueiro.cidade, cpf: @vaqueiro.cpf, data_nascimento: @vaqueiro.data_nascimento, estado: @vaqueiro.estado, nome: @vaqueiro.nome, tipo: @vaqueiro.tipo }
    assert_redirected_to vaqueiro_path(assigns(:vaqueiro))
  end

  test "should destroy vaqueiro" do
    assert_difference('Vaqueiro.count', -1) do
      delete :destroy, id: @vaqueiro
    end

    assert_redirected_to vaqueiros_path
  end
end
